#include <vector>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sstream>
#include <iostream>
#include <pthread.h>
#include "globals.h"
#include "file_manager.h"
#include "neighbors_vector.h"

using namespace ShareAll;

neighbors_vector neighbors;
file_manager fileManager;

void initGlobals(std::vector<std::string>& initialNeighbors) {
    neighbors = neighbors_vector(&initialNeighbors);
}
