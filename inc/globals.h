#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <string>
#include <vector>
#include "file_manager.h"
#include "neighbors_vector.h"

namespace ShareAll {
    static const int HOST_MAX_LENGTH = 255;
    static const int PORT_MAX_LENGTH = 5;
    static const int PORT_MINIMUM = 1024;
    static const int PORT_MAXIMUM = 65535;
    static const int PATH_MAX = 4096;
}
extern ShareAll::neighbors_vector neighbors;
extern ShareAll::file_manager fileManager;
extern void initGlobals(std::vector<std::string>& initialNeighbors);

#endif /* GLOBALS_H_ */
