#ifndef REQUEST_HANDLER_H_
#define REQUEST_HANDLER_H_

#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "service_request.h"

namespace ShareAll {
    class request_handler {
        private:
            struct worker_thread_args {
                int connection;
                int port;
            };
            int socketdesc;
            std::string port;
            int connection;
            bool stepToPort();
            static void* launchWorkerThread(void*);
        public:
            request_handler(int socketdesc, std::string port);
            void serviceRequests();
    };
}

#endif /* REQUEST_HANDLER_H_ */
