# Tentacles

Tentacles is a Distributed P2P file sharing software written in C++ for POSIX Compliant systems such as Linux, Mac.

# Installation

To compile the program, any C++ compiler that supports the C++11 standard will suffice. Use GNU Make to build the program. Run `make` in the root of the program's directory to start a build. Once complete, create a file named `allPorts.txt` inside of the `settings` directory. The first two lines should be used to specify an inclusive range of ports that the program may listen to, such as the following:

```
9800
10000
```

Optionally, you can create a file named `peers.txt` inside of the `settings` directory to specify peers that the program may contact. Each peer should be written on a separate line in the form `host:port`. If you do not create a file, the program will ask for a peer when it starts and create the file automatically. Below is an example:

```
localhost:98231
anotherNode:10928
```

Finally, place any files that you want to make available to other peers inside of the `sharing` directory.

# Usage

Run `bin/peer -c` to start the program and its command-line interface. If you want to start the program without its command-line interface (i.e. as a daemon), leave off the `-c` flag.

Once the program starts, run `help` to view a list of commands. If a command requires arguments, you can type in the command without arguments to view its usage.

